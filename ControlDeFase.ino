#include <WiFi.h>
#include <WebSocketServer.h>

#define LED 2
#define INT_PIN 25
#define TRIGGER_PIN 15

//SSID y pass de AP
const char* ssid = "ControlDeFase";
const char* pass = "87654321";

//Variables
boolean communicating = false;
int ledDelay = 200;
int counter = 0;
int percentage = 100;
unsigned long ledDelayTime;
unsigned long counterTime;
unsigned long trigger;

//Variables y funciones para interrupciones
volatile int interruptCounter = 0;

void IRAM_ATTR handleInterrupt()
{
  delayMicroseconds((int)trigger);
  digitalWrite(TRIGGER_PIN, LOW);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, HIGH);
}

//Servidor y WebSocket
WiFiServer server(80);
WebSocketServer webSocketServer;

//Cliente
WSiFiClient client;

//FUNCIONES
void setUpAP();
void handleWebSocketClient();
void setTriggerTime();

void setup()
{
  //Definir pin de led como salida
  pinMode(LED, 2);
  //Definir pin de interrupción como pullup interna
  pinMode(INT_PIN, INPUT);
  //Definir pin de disparo como salida
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, HIGH);
  //Amarrar pin de interrupción con su función
  attachInterrupt(digitalPinToInterrupt(INT_PIN), handleInterrupt, FALLING);

  //Inicializar puerto Serial
  Serial.begin(115200);

  //Configurar AP
  setUpAP();

  //Iniciar servidor
  server.begin();
}

void loop()
{
  if (millis() - ledDelayTime > ledDelay)
  {
    ledDelayTime = millis();
    digitalWrite(LED, !digitalRead(LED));
  }

  handleWebSocketClient();
  delayMicroseconds(100);
}


/** Funciones **/
void setUpAP()
{
  Serial.println("Configurando AP...");
  //Configurar ap
  WiFi.softAP(ssid, pass);

  //Obtener dirección IP
  IPAddress IP = WiFi.softAPIP();
  Serial.print("IP -> ");
  Serial.println(IP);
}

void handleWebSocketClient()
{
  //Checar si ya esta en comunicacion
  if (communicating)
  {
    if (!client.connected())
    {
      //Se desconectó el cliente
      Serial.println("Cliente DESconectado!");
      communicating = false;
      ledDelay = 200;
    }
    else
    {
      //Cadena para almacenar datos del cliente
      String mData;

      //Checar si hay algun dato disponible
      mData = webSocketServer.getData();

      if (mData.length() > 0)
      {
        Serial.println(mData);
        if (mData.indexOf("a") == 0)
        {
          //Obtener el valor del porcentaje
          percentage = (mData.substring(1)).toInt();
          setTriggerTime();
        }
        else
        {
          //Si no es un nuevo valor de porcentaje, hacer echo
          webSocketServer.sendData(mData);
        }
      }
    }
  }
  else
  {
    //Esta desconectado, buscar un cliente
    client = server.available();

    if (client.connected() && webSocketServer.handshake(client))
    {
      //Indicar que el cliente se conectó
      Serial.println("Cliente conectado");
      communicating = true;
      ledDelay = 500;
    }
  }
}

void setTriggerTime()
{
  Serial.print("Porcentaje -> ");
  Serial.print(percentage);
  Serial.println("%");

  //Para una señal de corriente directa pulsante el periodo dura 8.333 ms, siendo este el 100%
  //Para convertir de porcentaje a tiempo se utiliza la siguiente ecuacion
  // t = 8333 - (8333*p)/100

  trigger = 8333 - (int)((8333 * percentage) / 100);
  Serial.print("Tiempo de disparo -> ");
  Serial.print(trigger);
  Serial.println("us");
}

